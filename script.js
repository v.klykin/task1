function start () {
    var count = 0;
    var body = document.querySelector('body');
    var anotherTasks = [{name: "7", content: "", date: "2019.4.8, 14:55:30", number: 0, complected: false},
    {name: "3", content: "", date: "2019.4.8, 14:52:23", number: 2, complected: false},
    {name: "8", content: "", date: "2019.4.8, 14:22:24", number: 0, complected: false},
    {name: "1", content: "", date: "2019.4.8, 13:12:24", number: 4, complected: false},
    {name: "6", content: "", date: "2019.4.8, 13:12:18", number: 1, complected: false},
    {name: "2", content: "", date: "2019.4.8, 12:9:56", number: 3, complected: false}];
    var tasks = [];
    var headline = document.getElementById('headline');
    var button = document.getElementById('add');
    var headUp = document.getElementById('headUp');
    var headDown = document.getElementById('headDown');
    var timeUp = document.getElementById('timeUp');
    var timeDown = document.getElementById('timeDown');
    var content = document.getElementById('content');
    var body = document.querySelector('body');
    var textFilter = document.getElementById('filter');
    button.addEventListener('click', save);
    headUp.addEventListener('click', sortHeadUp);
    headDown.addEventListener('click', sortHeadDown);
    timeUp.addEventListener('click', sortTimeUp);
    timeDown.addEventListener('click', sortTimeDown);
    textFilter.addEventListener('keyup', filtration);

    if (localStorage.getItem('tasks') == null) {
        getAnotherTasks();
    }

    function getAnotherTasks () {
        var anotherTasksJson = JSON.stringify(anotherTasks);
        localStorage.setItem('tasks', anotherTasksJson);
        location.reload();
    }

    function filtration () {
        var count = 0;
        for (var i = 0; i < document.body.children.length; i++) {
                if (document.body.children[i].tagName === "DIV"){
                    count++
                 }
            }
        for (var l = 0; l < count; l ++) {
            body.removeChild(document.body.lastChild);
        }
          
        var loadedTasksJson = localStorage.getItem('tasks');
        var filterJson = JSON.parse(loadedTasksJson);
        var resultName;
        var resultContent;
        tasks = [];
        var textFilVal = textFilter.value.toLowerCase();

        function filtrationTasks (task) {
            var name = task.name.toLowerCase();
            var content = task.content.toLowerCase();
            resultName =  name.indexOf(textFilVal);
            resultContent = content.indexOf(textFilVal);
            return ((resultName > -1 || resultContent > - 1) || 
                   (resultName > -1 && resultContent > - 1))
            }
            for (var k = 0; k < filterJson.length; k++) {
                if (loadedTasksJson !== null) {
                    filterJson = filterJson.filter(filtrationTasks);
                    tasks.push(filterJson[k])
                    createDiv(filterJson[k]);
                }
           }
            
        }
                
                

    function sortTimeDown () {
        
        tasks = tasks.sort((a,b) => {
            aLower = a.date.toLowerCase();
            bLower = b.date.toLowerCase();
            return bLower.localeCompare(aLower);
        })
            
        var count = 0;
        for (var i = 0; i < document.body.children.length; i++) {
                if (document.body.children[i].tagName === "DIV"){
                    count++
                }
            }
        for (var l = 0; l < count; l ++) {
            body.removeChild(document.body.lastChild);
        }

        for (var k = 0; k < tasks.length; k++) {
            createDiv(tasks[k]);
        }
    }

    function sortTimeUp () {
        tasks = tasks.sort((a,b) => {
            aLower = a.date.toLowerCase();
            bLower = b.date.toLowerCase();
            return aLower.localeCompare(bLower);
        })

        var count = 0;
        for (var i = 0; i < document.body.children.length; i++) {
                if (document.body.children[i].tagName === "DIV"){
                    count++
                }
            }
        for (var l = 0; l < count; l ++) {
            body.removeChild(document.body.lastChild);
        }

        for (var k = 0; k < tasks.length; k++) {
            createDiv(tasks[k]);
        }

    }

     function sortHeadUp () {
        tasks = tasks.sort((a,b) => {
            aLower = a.name.toLowerCase();
            bLower = b.name.toLowerCase();
            return aLower.localeCompare(bLower);
        })

        var count = 0;
        for (var i = 0; i < document.body.children.length; i++) {
                if (document.body.children[i].tagName === "DIV"){
                    count++
                }
            }
        for (var l = 0; l < count; l ++) {
            body.removeChild(document.body.lastChild);
        }

        for (var k = 0; k < tasks.length; k++) {
            createDiv(tasks[k]);
        }
    }

    function sortHeadDown () {
        tasks = tasks.sort((a,b) => {
            aLower = a.name.toLowerCase();
            bLower = b.name.toLowerCase();
            return bLower.localeCompare(aLower);
        })
            
        var count = 0;
        for (var i = 0; i < document.body.children.length; i++) {
                if (document.body.children[i].tagName === "DIV"){
                    count++
                }
            }
        for (var l = 0; l < count; l ++) {
            body.removeChild(document.body.lastChild);
        }

        for (var k = 0; k < tasks.length; k++) {
            createDiv(tasks[k]);
        }
    }
    
    var loadedTasksJson = localStorage.getItem('tasks');
        if (loadedTasksJson !== null) {
            tasks = JSON.parse(loadedTasksJson);
            for (x = 0; x < tasks.length; x++) {
                tasks[x].number = x;
                createDiv(tasks[x]);
                    
            } 
        }
        
    

    function formatDate() {
        var today = new Date();
        var currDate = today.getDate();
        var currMonth = today.getMonth() + 1;
        var currYear = today.getFullYear();
        var currHours = today.getHours();
        var currMinutes = today.getMinutes();
        var currSeconds = today.getSeconds();
        return currYear + "." + currMonth + "." + currDate + ', ' + currHours + ':' + currMinutes + ':' +
        currSeconds;
    
    }
    function save () {
        var task = {
            name: headline.value,
            content: content.value,
            date: formatDate(),
            number: count,
            complected: false
        }
        tasks.push(task);
        count++;
        createDiv(task);

        var tasksJson = JSON.stringify(tasks);
        localStorage.setItem('tasks', tasksJson);
        location.reload()


    }

    function createDiv (task) {
        var div = document.createElement('div');
        div.style.width = '300px';
        div.style.height = '100px';
        div.style.border = '2px solid';
        div.style.marginBottom = '10px';
        body.appendChild(div);

        var headlineName = document.createTextNode('Заголовок: ');
        div.appendChild(headlineName);

       
        var headlineText = document.createTextNode(task.name);
        div.appendChild(headlineText);

        var br = document.createElement('br');
        div.appendChild(br);

        var contentName = document.createTextNode('Описание: ');
        div.appendChild(contentName);

        
        var contentText = document.createTextNode(task.content);
        div.appendChild(contentText);

        headline.value = '';
        content.value = '';

        var br = document.createElement('br');
        div.appendChild(br);

        var dateName = document.createTextNode('Время: ');
        div.appendChild(dateName);

        var dateText = document.createTextNode(task.date);
        div.appendChild(dateText);

        var br = document.createElement('br');
        div.appendChild(br);
        
        var buttonDel = document.createElement("button");
        buttonDel.innerText = 'Удалить';
        buttonDel.id = task.number;
        div.appendChild(buttonDel);
        
        var buttonEdit = document.createElement('button');
        buttonEdit.innerText = 'Редактировать';
        buttonEdit.id = task.number + 'edit';
        div.appendChild(buttonEdit);

        var checkComplected = document.createElement('input');
        checkComplected.type = 'checkbox';
        if (task.complected === true) {
            checkComplected.checked = 'checked';
        };
        div.appendChild(checkComplected);
        div.appendChild(document.createTextNode('Выполнено'));

        function checkbox (event) { 
            var changeComplected = localStorage.getItem('tasks');
            changeComplected = JSON.parse(changeComplected);         
            if (event.target.checked) {
                task.complected = true;
                changeComplected[task.number].complected = true;    
                } else {
                    task.complected = false;
                    changeComplected[task.number].complected = false;   
                }
            changeComplected = JSON.stringify(changeComplected);    
            localStorage.setItem('tasks', changeComplected);
        }
        checkComplected.addEventListener('change', checkbox)
        buttonDel.addEventListener('click', deletes);

        function deletes () {
            var deleteJson = localStorage.getItem('tasks');
            deleteJson = JSON.parse(deleteJson);
            deleteJson.splice(task.number, 1);
            deleteJson = JSON.stringify(deleteJson);    
            localStorage.setItem('tasks', deleteJson);

            body.removeChild(this.parentNode);
            location.reload()
            
        }

        function edit () {
            var divEdit = this.parentNode;
            var EditJson = localStorage.getItem('tasks');
            EditJson = JSON.parse(EditJson);
            EditJson.splice(task.number, 1);
            EditJson = JSON.stringify(EditJson);    
            localStorage.setItem('tasks', EditJson);

            body.removeChild(this.parentNode);
            headline.value = divEdit.childNodes[1].data;
            content.value = divEdit.childNodes[4].data;
            tasks.splice(task.number, 1);

        }
        buttonDel.addEventListener('click', deletes);
        buttonEdit.addEventListener('click', edit)
    }
}
document.addEventListener('DOMContentLoaded', start);
